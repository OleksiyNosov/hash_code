file_data = []

# fname = 'a_example'
# fname = 'b_should_be_easy'
# fname = 'c_no_hurry'
# fname = 'd_metropolis'
fname = 'e_high_bonus'

File.open(fname + '.in', "r") do |f|
  f.each_line do |line|
    file_data.push(line)
  end
end

file_data.each { |l| puts l }

main_data = file_data.shift.split.map { |d| d.to_i }

h_main_data = {
  rows: main_data[0],
  cols: main_data[1],
  cars: main_data[2],
  rides: main_data[3],
  bonus: main_data[4],
  steps: main_data[5]
}

$rows = main_data[0]
$cols = main_data[1]
$cars = main_data[2]
$rides = main_data[3]
$bonus = main_data[4]
$steps = main_data[5]

h_main_data.each { |k, v| puts "#{ k } = #{ v }" }

$rides_data = file_data.each_with_index.map { |l, id| l = l.split.map { |n| n.to_i }; { id: id, sx: l[0], sy: l[1], fx: l[2], fy: l[3], s: l[4], f: l[5] } }

$rides_data.each { |r| puts(r.map { |k, v| "#{ k }: #{ v } " }.join) }

$cars_pull = (0...$cars).each_with_index.map { |c, i| { id: i, x: 0, y: 0, rides: [] } }

def calc_time sx, sy, fx, fy
  ((fy - sy).abs + (fx - sx).abs)
end

def drive ci
  car = $cars_pull[ci]

  time = 0

  ride = $rides_data.first

  return unless ride

  time += calc_time(ride[:sx], ride[:sy], car[:x], car[:y])

  car[:x] = ride[:sx]
  car[:y] = ride[:sy]

  if time <= ride[:s]
    time = ride[:s]
  end

  time += calc_time(ride[:fx], ride[:fy], car[:x], car[:y])

  car[:x] = ride[:fx]
  car[:y] = ride[:fy]

  if time <= $steps
    car[:rides].push ride[:id]

    $rides_data.shift
  elsif ride[:f] < time
    return
  else
    return
  end
end

while $rides_data.count > 0
(0...$cars).each do |ci|
  drive ci
end
end

puts $cars_pull.map { |c| "#{c[:id] + 1} x: #{c[:x]} y: #{c[:y]} #{c[:rides].join(' ')}" }

puts $rides_data.count

File.open(fname + '.out', 'w') do |file|
  $cars_pull.each do |c|
    file.write("#{c[:rides].count} #{c[:rides].join(' ')}\n")
  end
end
