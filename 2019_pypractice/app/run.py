import os
import numpy as np

file_name = 'a_example'
# file_name = 'b_small'
# file_name = 'c_medium'
# file_name = 'd_big'
file_path = os.path.join("dataset", file_name)

def load_data(file_path):
	file = open(f'{file_path}.in', "r")
	pizza = np.array(file.read().splitlines())
	file.close()

	header = pizza[0].copy()
	pizza  = pizza[1:]

	rows, cols, min_ing, max_cel = list(map(int, header.split()))
	pizza = np.array(list(map(lambda row: list(row), pizza)))
	return [rows, cols, min_ing, max_cel, pizza]

def print_headers(rows, cols, min_ing, max_cel):
	print("Rows:", rows)
	print("Columns:", cols)
	print("Min ingridients:", min_ing)
	print("Max cells per slice:", max_cel)

def save_result(file_path, data):
	file = open(f'{file_path}.out', "w")
	file.write(f'{len(data)}\n')
	for row in data:
		out_row = ' '.join(map(str, row))
		file.write(f'{out_row}\n')
	file.close()

class Pizza:
	def __init__(self, **kwargs):
		self.pizza   = kwargs['pizza']
		self.rows    = kwargs['rows']
		self.cols    = kwargs['cols']
		self.min_ing = kwargs['min_ing']
		self.max_cel = kwargs['max_cel']

		self.taken_cells = []

	def is_on_pizza(self, pos):
		return 0 <= pos[0] and pos[0] < self.rows and 0 <= pos[1] and pos[1] < self.cols

	def is_taken(self, pos):
		return pos in

	def neighbors(self, pos):
		local_neighbors = []

		for row in range(pos[0]-1, pos[0]+1):
			for col in range(pos[1]-1, pos[1]+1):
				neighbor = (row, col)

				if is_on_pizza((row, col)) and neighbor != pos:
					local_neighbors.append(neighbor)

		return local_neighbors

	def positions(self):
		all_positions = []

		for row_idx in range(len(self.pizza)):
			for col_idx in range(len(self.pizza[row_idx])):
				all_positions.append((row_idx, col_idx))

		return all_positions




rows, cols, min_ing, max_cel, pizza = load_data(file_path)
print_headers(rows, cols, min_ing, max_cel)
print(pizza[:6])

pizza = Pizza(pizza=pizza, rows=rows, cols=cols, min_ing=min_ing, max_cel=max_cel)

for row, col in pizza.positions():
	for neighbor in pizza.neighbors((row, col)):



example_out = [
	[0, 0, 2, 1],
	[0, 2, 2, 2],
	[0, 3, 2, 4]
]

save_result(file_path, example_out)
