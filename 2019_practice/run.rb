require 'set'

class LoadingMessage
  attr_accessor :min, :max, :step, :message, :loading_index, :finish_message, :start_time, :finish_time

  def initialize(min: 0, max: 100, step: 1, message: 'Working:', finish_message: 'Done.')
    @min = min
    @max = max
    @step = step
    @message = message
    @finish_message = finish_message

    @loading_index = 0.0
  end

  def increment
    @loading_index += 1
    @start_time ||= Time.now

    printf("\r#{ message } %.2f%", loading_index / max * 100)
  end

  def finish
    rows = [
      started_at,
      finished_at,
      time_took,
      finish_message
    ]

    message = rows.join("\n")

    puts "\n#{ message }"
  end

  private

  def time_took
    delta = finish_time - start_time

    "Time: #{ delta } seconds."
  end

  def started_at
    "Started at: #{ start_time }"
  end

  def finished_at
    "Finished at: #{ finish_time }"
  end

  def finish_time
    @finish_time ||= Time.now
  end
end

class Pizza
  attr_accessor :raw_pizza, :cells, :width, :height, :fillings, :slices, :sliced_cells

  def initialize raw_pizza:, width:, height:
    @raw_pizza = raw_pizza
    @width = width
    @height = height
    @fillings = ['T', 'M']
    @slices = []
    @sliced_cells = Set.new

    init_pizza
  end

  def get_cell x, y
    return if not in_bounds?(x, y)

    cells[x + y * width]
  end

  def sliced? cell
    return false if slices.empty?

    slices.any? { |slice| slice.include?(cell) }
  end

  def valid_slice? slice
    counted_fillings = count_fillings(slice)

    return false if counted_fillings[:T].zero? || counted_fillings[:M].zero?

    return false if not available_cells?(slice.cells)

    true
  end

  def available_cells? cells
    !cells.any? { |cell| sliced_cells.include?(cell) }
  end

  def count_fillings(slice)
    counter = {T: 0, M: 0}

    get_cells_from_range(slice.columns, slice.rows).each do |cell|
      counter[cell.filling] += 1
    end

    counter
  end

  def add_slice slice
    slices.push slice

    @sliced_cells += slice.cells
  end

  def neighbors cell
    columns = (cell.x - 1)..(cell.x + 1)
    rows    = (cell.y - 1)..(cell.y + 1)

    get_cells_from_range(columns, rows).select { |neighbor| neighbor != cell }
  end

  def get_cells_from_slice(slice)
    get_cells_from_range(slice.columns, slice.rows)
  end

  def expand_slice slice
    expand_data_set = slice.expand_data_set

    expand_data_set.each do |expand_data|
      point   = expand_data[:point]
      columns = expand_data[:columns]
      rows    = expand_data[:rows]

      expand_data[:new_cells] = new_cells = get_cells_from_range(columns, rows)

      expand_data[:cell] = get_cell(point.x, point.y)

      # puts "new cells: #{ new_cells.to_s }"
      # puts "available: #{ available_cells? new_cells }"

      expand(slice, expand_data) if !new_cells.empty? && available_cells?(new_cells)
    end
  end

  private

  def expand(slice, expand_data)
    slice.expand(expand_data)

    @sliced_cells += expand_data[:new_cells]
  end

  def get_cells_from_range(columns, rows)
    result = []

    rows.each do |j|
      columns.each do |i|
        result.push get_cell(i, j)
      end
    end

    result.compact
  end

  def in_bounds?(x, y)
    0 <= x && x < width && 0 <= y && y < height
  end

  def init_pizza
    @cells = []

    raw_pizza.each_with_index do |row, j|
      row.split('').each_with_index do |raw_cell, i|
        next if not fillings.include?(raw_cell)

        new_cell = Cell.new(filling: raw_cell.to_sym, position: Point.new(x: i, y: j))

        cells.push new_cell
      end
    end
  end
end

class Slice
  attr_accessor :start_cell, :end_cell, :cells

  def initialize start_cell:, end_cell:, cells: nil
    @start_cell = start_cell
    @end_cell = end_cell
    @cells = cells

    reorder_cells
  end

  def expand_left
    start_point = Point.new(x: start_cell.x - 1, y: start_cell.y)
    end_point   = Point.new(x: end_cell.x, y: end_cell.y)

    columns = (start_point.x..start_point.x).to_a
    rows    = (start_point.y..end_point.y).to_a

    {point: start_point, cell_type: :start_cell, columns: columns, rows: rows}
  end

  def expand_up
    start_point = Point.new(x: start_cell.x, y: start_cell.y - 1)
    end_point   = Point.new(x: end_cell.x, y: end_cell.y)

    columns = (start_point.x..end_point.x).to_a
    rows    = (start_point.y..start_point.y).to_a

    {point: start_point, cell_type: :start_cell, columns: columns, rows: rows}
  end

  def expand_right
    end_point   = Point.new(x: end_cell.x + 1, y: end_cell.y)
    start_point = Point.new(x: start_cell.x, y: start_cell.y)

    columns = (end_point.x..end_point.x).to_a
    rows    = (start_point.y..end_point.y).to_a

    {point: end_point, cell_type: :end_cell, columns: columns, rows: rows}
  end

  def expand_down
    end_point   = Point.new(x: end_cell.x, y: end_cell.y + 1)
    start_point = Point.new(x: start_cell.x, y: start_cell.y)

    columns = (start_point.x..end_point.x).to_a
    rows    = (end_point.y..end_point.y).to_a

    {point: end_point, cell_type: :end_cell, columns: columns, rows: rows}
  end

  def expand_data_set
    [expand_down, expand_right, expand_up, expand_left]
  end

  def expand expand_data
    cells.push expand_data[:new_cells]

    self.send("#{ expand_data[:cell_type] }=", expand_data[:cell])
  end

  def include? cell
    (x1 <= cell.x && cell.x <= x2) && (y1 <= cell.y && cell.y <= y2)
  end

  def overlap? other
    !(rows & other.rows).empty? && !(columns & other.columns).empty?
  end

  def x1; start_cell.position.x end
  def y1; start_cell.position.y end
  def x2; end_cell.position.x end
  def y2; end_cell.position.y end

  def columns; (x1..x2).to_a end
  def rows; (y1..y2).to_a end

  def to_s
    "{(#{x1};#{y1}),(#{x2};#{y2})}"
  end

  def raw_s
    "#{y1} #{x1} #{y2} #{x2}"
  end

  private

  def reorder_cells
    if end_cell.position.x <= start_cell.position.x && end_cell.position.y <= start_cell.position.y
      tmp         = start_cell
      @start_cell = end_cell
      @end_cell   = tmp
    end
  end
end

class Cell
  attr_accessor :filling, :position

  def initialize(filling:, position:)
    @filling = filling
    @position = position
  end

  def ==(other)
    x == other.x && y == other.y && filling == other.filling
  end

  def to_s
    "(#{x};#{y};#{filling})"
  end

  def inspect
    to_s
  end

  def x; position.x end
  def y; position.y end
end

class Point
  attr_accessor :x, :y

  def initialize x:, y:
    @x = x
    @y = y
  end

  def to_s
    "(#{x};#{y})"
  end
end

def run(fname)
  file_data = []
  data_path = '2019_practice/'

  File.open(data_path + 'data/' + fname + '.in', 'r') do |f|
    f.each_line do |line|
      file_data.push(line)
    end
  end

  rows, columns, min_ing, max_cells = file_data.shift.split.map(&:to_i)

  pizza = Pizza.new(raw_pizza: file_data, width: columns, height: rows)

  puts
  puts "Pizza: #{ fname }"
  puts "rows: #{ rows }"
  puts "columns: #{ columns }"
  puts "min_ing: #{ min_ing }"
  puts "max_cells: #{ max_cells }"

  loading_message = LoadingMessage.new(max: pizza.cells.count)

  pizza.cells.each do |cell|
    loading_message.increment

    next if pizza.sliced?(cell)

    pizza.neighbors(cell).each do |neighbor_cell|
      new_slice = Slice.new(start_cell: cell, end_cell: neighbor_cell)
      new_slice.cells = pizza.get_cells_from_slice(new_slice)

      next unless pizza.valid_slice?(new_slice)

      pizza.add_slice(new_slice)

      break
    end
  end

  2.times do
    loading_message.max += pizza.slices.count

    pizza.slices.each do |slice|
      loading_message.increment

      pizza.expand_slice(slice)
    end
  end

  File.open(data_path + fname + '.out', 'w') do |file|
    file.puts pizza.slices.count

    pizza.slices.each do |slice|
      file.puts slice.raw_s
    end
  end

  loading_message.finish
end

# fnames = ['a_example', 'b_small', 'c_medium', 'd_big']
fnames = ['a_example', 'b_small']
# fnames = ['a_example']

fnames.each { |fname| run(fname) }














