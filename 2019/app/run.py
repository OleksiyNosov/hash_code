import os
import numpy as np

class TagBook:
	def __init__(self):
		self.tag_book = {}
		self.counter = 0

	def encode(self, raw_tags):
		result = set()

		for tag in raw_tags:
			if tag not in self.tag_book:
				self.tag_book[tag] = self.counter
				self.counter += 1

			result.add(self.tag_book[tag])

		return result

class PhotoBook:
	def __init__(self):
		self.photo_scores = {}

	def combined_id(self, left_photo, right_photo):
		return f'({left_photo.id};{right_photo.id})'

	def is_calculated(self, left_photo, right_photo):
		if self.combined_id(left_photo, right_photo) in self.photo_scores:
			return True
		if self.combined_id(right_photo, left_photo) in self.photo_scores:
			return True

		return False

	def update_score(self, left_photo, right_photo):
		score = self.calc_cscore(left_photo, right_photo)

		self.photo_scores[self.combined_id(left_photo, right_photo)] = score
		self.photo_scores[self.combined_id(right_photo, left_photo)] = score

	def cscore(self, left_photo, right_photo):
		if not self.is_calculated(left_photo, right_photo):
			self.update_score(left_photo, right_photo)

		return self.photo_scores[self.combined_id(left_photo, right_photo)]

	def calc_cscore(self, left_photo, right_photo):
		t_left = left_photo.tags - right_photo.tags
		if len(t_left) == 0:
			return 0

		t_right = right_photo.tags - left_photo.tags
		if len(t_right) == 0:
			return 0

		t_join = left_photo.tags & right_photo.tags
		if len(t_join) == 0:
			return 0

		return min([len(t_join), len(t_left), len(t_right)])

class Photo:
	def __init__(self, id, raw_photo, photo_book):
		self.id = str(id)
		raw_data = raw_photo.split(' ')
		self.pos = raw_data[0]
		self.original_tags = set(raw_data[2:])
		self.photo_book = photo_book

	def print(self):
		print("Id:", self.id, self.pos, str(self.tags))

	def is_vertical(self):
		return self.pos == 'V'

	def is_horizontal(self):
		return self.pos == 'H'

	def merge(self, other):
		self.id = f'{self.id} {other.id}'
		self.tags = self.tags | other.tags
		self.pos = 'VV'

	def encode_tags(self, tag_book):
		self.tags = tag_book.encode(self.original_tags)

	def cscore(self, other):
		return self.photo_book.cscore(self, other)

# class SlideMaker:
# 	def __init__(self):
# 		pass

# 	def make_slides(self, photos):
# 		slides = []

# 		for i in range(len(photos)):
# 			trg_idx, trg_idx =

# 			for j in range(1, len(photos)):

class Chain:
	def __init__(self, photo):
		self.photos = [photo]

	def is_match_first(self, photo):
		return self.photos[0].cscore(photo) > 0

	def is_match_last(self, photo):
		return self.photos[-1].cscore(photo) > 0

	def append_first(self, photo):
		self.photos.insert(0, photo)

	def append_last(self, photo):
		self.photos.append(photo)

	def is_merge_first(self, chain):
		return self.photos[0].cscore(chain.photos[-1]) > 0

	def is_merge_last(self, chain):
		return self.photos[-1].cscore(chain.photos[0]) > 0

	def merge_first(self, chain):
		self.photos = chain.photos + self.photos

	def merge_last(self, chain):
		self.photos = self.photos + chain.photos

	def ids(self):
		return list(map(lambda p: p.id, self.photos))

	def len(self):
		return len(self.photos)

	def pos_score(self, idx, photo):
		score = 0
		if idx-1 > 0:
			score += photo.cscore(self.photos[idx-1])
		if idx+1 < self.len():
			score += photo.cscore(self.photos[idx+1])

		return score

	def should_swap(self, left_idx, right_idx):
		i, j = left_idx, right_idx

		org_lt, org_rt = self.pos_score(i, self.photos[i]), self.pos_score(j, self.photos[j])
		swp_lt, swp_rt = self.pos_score(i, self.photos[j]), self.pos_score(j, self.photos[i])

		return swp_lt + swp_rt > org_lt + org_rt

	def swap(self, left_idx, right_idx):
		self.photos[left_idx], self.photos[right_idx] = self.photos[right_idx], self.photos[left_idx]

	def sort(self):
		is_max_iter = True
		max_iter = 10_000_000
		max_len = int((len(self.photos)) / 2 * len(self.photos))

		if is_max_iter and max_len > max_iter:
			print("Times Bigger:", int(max_len / max_iter))
			max_len = max_iter

		current_index, current_length = 0, max_len

		for i in range(len(self.photos)):
			for j in range(i + 1, len(self.photos)):
				current_index += 1
				print(f'\rSorting: {current_index / current_length * 100}', end='')
				if self.should_swap(i, j):
					self.swap(i, j)
				if current_index > max_len:
					return


def run(file_name):
	file_path = os.path.join("datasets", file_name)
	file = open(f'{file_path}.txt', "r")
	raw_data = file.read().splitlines()
	length = header = int(raw_data[0])
	raw_data = raw_data[1:]

	data = []
	chains = []
	tag_book = TagBook()
	photo_book = PhotoBook()
	# slide_maker = SlideMaker()

	print("Dataset:", file_name)

	current_index, current_length = 0, length
	for i in range(length):
		current_index += 1
		print(f'\rParsing: {current_index / current_length * 100}', end='')
		photo = Photo(i, raw_data[i], photo_book)
		photo.encode_tags(tag_book)
		data.append(photo)
	print()

	photos = []
	photos_vertical = []

	for photo in data:
		if photo.is_horizontal():
			photos.append(photo)
		else:
			photos_vertical.append(photo)

	for i in range(0, len(photos_vertical), 2):
		if i + 1 >= len(photos_vertical):
			break
		photos_vertical[i].merge(photos_vertical[i + 1])
		photos.append(photos_vertical[i])

	current_index, current_length = 0, len(photos)

	for photo in photos:
		current_index += 1
		print(f'\rPhotos: {current_index / current_length * 100}', end='')

		added_to_chain = False

		if photo.is_vertical():
			continue

		for chain in chains:
			if file_name == "b_lovely_landscapes":
				chain.append_first(photo)
				added_to_chain = True
				break

			if chain.is_match_first(photo):
				chain.append_first(photo)
				added_to_chain = True
				break

			if chain.is_match_last(photo):
				chain.append_last(photo)
				added_to_chain = True
				break

		if not added_to_chain:
			chains.append(Chain(photo))
	print()

	chains_l2 = [chains[0]]

	current_index, current_length = 1, len(chains)
	for chain in chains[1:]:
		current_index += 1
		print(f'\rChains: {current_index / current_length * 100}', end='')
		added_to_chain = False

		for chain_l2 in chains_l2:
			if chain_l2.is_merge_first(chain):
				chain_l2.merge_first(chain)
				added_to_chain = True
				break

			if chain_l2.is_merge_last(chain):
				chain_l2.merge_last(chain)
				added_to_chain = True
				break
	if len(chains_l2) > 1:
		print()

	result = []

	for c in chains_l2:
		c.sort()
		result += c.ids()

	file = open(f'app/{file_name}.out', "w")
	file.write(f'{len(result)}\n')
	for id in result:
		file.write(f'{id}\n')

	print("\n")

file_names = [
	"a_example",
	"b_lovely_landscapes",
	"c_memorable_moments",
	"d_pet_pictures",
	"e_shiny_selfies"
]

for file_name in file_names:
	run(file_name)

